# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import Maten
from .models import Explanation
from .models import MatenFiles


admin.site.register(Maten)
admin.site.register(Explanation)
admin.site.register(MatenFiles)
