from __future__ import unicode_literals

from django.apps import AppConfig


class MotounConfig(AppConfig):
    name = 'forkan.motoun'
    verbose_name = 'Forkan'
