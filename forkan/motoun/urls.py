# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf import settings
from django.conf.urls import include, url
from django.views.generic import TemplateView

from .views import home, download_handler


urlpatterns = [
    url(r'^$', home, name='motoun_home'),
    url(r'^download/(?P<pk>[0-9]+)$', download_handler,
        name='motoun_download_handler'),
]
