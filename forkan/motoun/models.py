from __future__ import unicode_literals

import os

from django.db import models
from django.utils.translation import gettext_lazy as _
from django.utils.encoding import python_2_unicode_compatible


from forkan.utils.models import MetaAbstractModel


def get_upload_path(instance, filename):
    return os.path.join(instance.explanation.maten.slug,
                        instance.explanation.slug,
                        filename)


@python_2_unicode_compatible
class Maten(MetaAbstractModel):
    title = models.CharField(_("Title"), max_length=100)
    slug = models.SlugField(_("Slug"))
    author = models.CharField(_("Author"), max_length=200)

    def __str__(self):
        return self.slug


@python_2_unicode_compatible
class Explanation(MetaAbstractModel):
    maten = models.ForeignKey(Maten, verbose_name=_('Maten'))
    slug = models.SlugField(_("Slug"))
    commentator = models.CharField(_("Commentator"), max_length=200)
    start_date = models.DateField(_("Start date"), auto_now_add=True)

    def __str__(self):
        return '{}/{}'.format(self.maten, self.slug)


@python_2_unicode_compatible
class MatenFiles(MetaAbstractModel):
    explanation = models.ForeignKey(Explanation, verbose_name=_('Explanation'))
    title = models.CharField(_("Title"), max_length=100)
    audio = models.FileField(_("Audio file"), upload_to=get_upload_path)

    # def save(self, *args, **kwargs):
    #     super(MatenFiles, self).save(*args, **kwargs)
    #     filename = self.data.url
    #     # Do anything you'd like with the data in filename

    def __str__(self):
        return '{}/{}'.format(self.explanation, self.title)
