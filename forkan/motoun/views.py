from django.shortcuts import render
from django.conf import settings
from django.shortcuts import get_object_or_404

from filetransfers.api import serve_file

from .models import Maten
from .models import Explanation
from .models import MatenFiles


def home(request):
    files = MatenFiles.objects.select_related()
    context = {
        'files': files
    }
    return render(request, 'motoun/home.html', context=context)


def download_handler(request, pk):
    upload = get_object_or_404(MatenFiles, pk=pk)
    return serve_file(request, upload.audio)
