from django.conf import settings
from django.db import models
from django.utils.translation import gettext_lazy as _


class MetaAbstractModel(models.Model):

    created_at = models.DateTimeField(_("Created at"), auto_now_add=True, editable=False)
    last_updated_at = models.DateTimeField(_("Last updated at"), auto_now=True, editable=False)

    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True,
                                   related_name="%(class)s_created_set", editable=False,
                                   verbose_name=_("Created by"))
    last_updated_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True,
                                        related_name="%(class)s_last_updated_set", editable=False,
                                        verbose_name=_("Last updated by"))

    class Meta:
         abstract = True
